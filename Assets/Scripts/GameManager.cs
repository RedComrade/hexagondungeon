﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    private const string BOARD_SCENE_NAME = "BoardScene";
    private const string MAP_SCENE_NAME = "MapScene";

    public static GameManager instance;
    private Room currentRoom;
    private Room destinationRoom;
    private List<Room> rooms;
    private int roomNumber;

    void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    static public void CallbackInitialization() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private static void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode) {
        switch (scene.name) {
            case BOARD_SCENE_NAME:
                instance.SetupBoardScene();
                break;
            case MAP_SCENE_NAME:
                instance.SetupMapScene();
                break;
        }
    }

    private void SetupBoardScene() {
        BoardManager.instance.GenerateBoardAndStartTurns();
    }

    private void SetupMapScene() {
        MapInputManager.instance.RoomClickedListeners += MapManager.instance.OnRoomClicked;
        if (rooms.Count == 0) {
            MapManager.instance.GenerateRooms();
        } else {
            List<Room> newRooms = new List<Room>();
            foreach (Room room in rooms) {
                if (room == currentRoom) {
                    room.roomType = RoomType.VISITED;
                } else if (room == destinationRoom) {
                    room.roomType = RoomType.CURRENT;
                }
                newRooms.Add(room);
            }
            MapManager.instance.LoadRooms(newRooms);
        }
    }

    public void TravelTo(Room currentRoom, Room destinationRoom, List<Room> rooms) {
        this.roomNumber++;
        this.currentRoom = currentRoom;
        this.destinationRoom = destinationRoom;
        this.rooms = rooms;
        SceneManager.LoadScene(BOARD_SCENE_NAME, LoadSceneMode.Single);
    }

    public void PlayClicked() {
        roomNumber = 0;
        rooms = new List<Room>();
        SceneManager.LoadScene(MAP_SCENE_NAME, LoadSceneMode.Single);
    }

    public void ExitFromRoom() {
        SceneManager.LoadScene(MAP_SCENE_NAME, LoadSceneMode.Single);
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardInputManager : MonoBehaviour {

    public static BoardInputManager instance;

    public LayerMask layerIDForHexTiles;

    public delegate void OnHexClicked(Hex hex);
    public event OnHexClicked HexClickedListeners;


    void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
    }

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            Hex hex = GetHexUnderMouse();
            if (hex != null) {
                //Debug.Log("Clicked hex " + hex.Q + " " + hex.R);
                HexClickedListeners.Invoke(hex);
            }
        }
    }

    private Hex GetHexUnderMouse() {
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePosition2D = new Vector2(mousePosition.x, mousePosition.y);
        RaycastHit2D hit = Physics2D.Raycast(mousePosition2D, Vector2.zero, layerIDForHexTiles.value);
        if (hit.collider != null) {
            GameObject hexGO = hit.collider.gameObject; // The collider is a child of the "correct" game object that we want.
            return BoardManager.instance.GetHexFromGameObject(hexGO);
        }
        return null;
    }

}

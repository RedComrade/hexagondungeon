﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Pair<T> {

    public T first { get; private set; }
    public T second { get; private set; }

    public Pair(T first, T second) {
        this.first = first;
        this.second = second;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    private static readonly float SPEED = 20f;
    private Rigidbody2D rb2D;
    private const float DISTANCE_PRESICION = 1f;

    protected void Awake() {
        rb2D = GetComponent<Rigidbody2D>();
    }

    public IEnumerator MoveTo(Vector3 end) {
        float sqrRemainingDistance = (transform.position - end).sqrMagnitude;

        while (sqrRemainingDistance > DISTANCE_PRESICION) {
            Vector3 newPostion = Vector3.MoveTowards(rb2D.position, end, SPEED * Time.deltaTime);
            rb2D.MovePosition(newPostion);
            sqrRemainingDistance = (transform.position - end).sqrMagnitude;
            yield return null;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit {

    private Hex clickedHex = null;
    private Hex[] path = new Hex[0];

    protected override void Start() {
        BoardInputManager.instance.HexClickedListeners += HexClicked;
        base.Start();
    }

    public override IEnumerator MakeTurn() {
        while (clickedHex == null || !InstantiatePathIfPossible(clickedHex)) {
            clickedHex = null;
            yield return new WaitForEndOfFrame();
        }

        //Debug.Log("After can move");
        Unit unitToAttack;
        Hex[] actualPath;
        AnalyzePath(path, out unitToAttack, out actualPath);
        yield return base.Move(actualPath);

        if (unitToAttack == null) {
            Unit[] unitsInAttackArea = BoardManager.instance.GetUnitsInAttackArea(hex, attackType, range, UnitType.ENEMY);
            if (unitsInAttackArea.Length > 0) {
                clickedHex = null;
                while (clickedHex == null) {
                    yield return new WaitForEndOfFrame();
                }
                if (clickedHex.unit != null && clickedHex.unit.unitType != UnitType.PLAYER) {
                    unitToAttack = clickedHex.unit;
                }
            }
        }
        if (unitToAttack != null) {
            FlipSpriteIfNeeded(hex, unitToAttack.hex);
            BoardOutputManager.instance.OnPlayerAttacks();
            unitToAttack.TakeDamage(damage);
        }

        clickedHex = null;
        yield return WaitForTurnEndDelay();
    }

    private void HexClicked(Hex hex) {
        clickedHex = hex;
    }

    public override void TakeDamage(int damage) {
        Debug.Log("Take damage " + health + " -" + damage);
        health -= damage;
        if (health <= 0) {
            BoardManager.instance.OnUnitDies(this);
            enabled = false;
            Destroy(gameObject);
            BoardManager.instance.OnGameOver();
            BoardOutputManager.instance.OnGameOver();
        }
    }

    private bool InstantiatePathIfPossible(Hex destinationHex) {
        path = FindPath(destinationHex);
        if (path.Length == 0) {
            Debug.LogWarning("Unrichable path!");
            return false;
        }
        if (path.Length > speed + 1) { // +1 because initial hex counts as part of path path
            Debug.LogWarning("Not enough speed to pass the path!");
            return false;
        }
        if (clickedHex.unit == this) {
            Debug.LogWarning("Player can't attack himself!");
            return false;
        }
        return true;
    }
}

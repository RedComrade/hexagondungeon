﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MapManager : MonoBehaviour {

    public static MapManager instance;

    public Room currentRoom;
    private Room selectedRoom;

    public float width;
    public float height;
    public float radius;
    private float neighborRadius;

    private List<Room> rooms;
    private Dictionary<Room, GameObject> roomsToGameObject;
    private Dictionary<GameObject, Room> gameObjectToRoom;
    private List<GameObject> lines;
    private PoissonDiscSampler sampler;


    void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
        neighborRadius = radius * 2;
    }

    public void GenerateRooms() {
        roomsToGameObject = new Dictionary<Room, GameObject>();
        gameObjectToRoom = new Dictionary<GameObject, Room>();
        rooms = new List<Room>();
        lines = new List<GameObject>();
        sampler = new PoissonDiscSampler(width, height, radius);
        foreach (Vector2 sample in sampler.Samples()) {
            Room room = new Room(sample.x, sample.y);
            room.roomType = RoomType.UNKNOWN;
            rooms.Add(room);
        }
        currentRoom = Utils.MinBy(rooms, room => room.x);
        currentRoom.roomType = RoomType.CURRENT;
        Utils.MaxBy(rooms, room => room.x).roomType = RoomType.EXIT;
        foreach (Room room in rooms) {
            InitializeRoom(room);
        }
        OnRoomClicked(currentRoom); // show and initialize neighbor
    }

    public void LoadRooms(List<Room> roomsToLoad) {
        roomsToGameObject = new Dictionary<Room, GameObject>();
        gameObjectToRoom = new Dictionary<GameObject, Room>();
        rooms = roomsToLoad;
        lines = new List<GameObject>();
        foreach (Room room in roomsToLoad) {
            InitializeRoom(room);
            if (room.roomType == RoomType.CURRENT) {
                currentRoom = room;
            }
        }
        OnRoomClicked(currentRoom); // show and initialize neighbor
    }

    public void OnRoomClicked(Room clickedRoom) {
        if (clickedRoom == selectedRoom && currentRoom.HasNeighbour(clickedRoom)) {
            GameManager.instance.TravelTo(currentRoom, clickedRoom, rooms);
            return;
        }
        selectedRoom = clickedRoom;
        if (selectedRoom.neighbours == null) {
            List<Room> neighborRooms = new List<Room>();
            foreach (Room room in rooms) {
                if (clickedRoom != room && room.IsInRange(clickedRoom, neighborRadius)) {
                    neighborRooms.Add(room);
                }
            }
            selectedRoom.neighbours = neighborRooms;
        }
        DestroyLines();
        foreach (Room neighborRoom in selectedRoom.neighbours) {
            GameObject lineGO = MapFabric.instance.CreateLine(clickedRoom.x, clickedRoom.y);
            lines.Add(lineGO);
            LineRenderer lineRenderer = lineGO.GetComponent<LineRenderer>();
            lineRenderer.SetPosition(0, new Vector2(clickedRoom.x, clickedRoom.y));
            lineRenderer.SetPosition(1, new Vector2(neighborRoom.x, neighborRoom.y));
        }
    }

    public void OnEmptySpaceClicked() {
        // erase lines
    }

    public Room GetRoomFromGameObject(GameObject roomGO) {
        if (gameObjectToRoom.ContainsKey(roomGO)) {
            return gameObjectToRoom[roomGO];
        }
        return null;
    }

    public GameObject GetGameObjectFromRoom(Room room) {
        if (roomsToGameObject.ContainsKey(room)) {
            return roomsToGameObject[room];
        }
        return null;
    }

    private void DestroyLines() {
        foreach (GameObject line in lines) {
            Destroy(line);
        }
        lines.Clear();
    }

    private void InitializeRoom(Room room) {
        GameObject roomGO = MapFabric.instance.CreateRoom(room);
        roomsToGameObject.Add(room, roomGO);
        gameObjectToRoom.Add(roomGO, room);
    }

}
